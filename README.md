# Sokoban3D

3D remake, developed with Unreal Engine 4!

To edit map, cell grid needs to be generated first (button with settings in the
SokobanMap\`s Details panel). Next, configure each tile individually
(select cell type from enum in SokobanCell\`s instance Details panel and
Construction Script will do the rest)

### Game Controls:
* WASD - movement
* C - undo move
* R - reset level
* Q - quit
