// Fill out your copyright notice in the Description page of Project Settings.

#include "SokobanGame.h"

#include "SokobanMap.h"

ASokobanGame::ASokobanGame()
{
	PrimaryActorTick.bCanEverTick = true;

	DefaultPawnClass = nullptr;

}

void ASokobanGame::Tick(float DeltaSeconds)
{
	int32 CurrentScore = 0;

	for (ASokobanBox* Box : Map->Boxes)
	{
		if (Box->bIsInPlace) CurrentScore++;
	}

	Score = CurrentScore;

}
