// Fill out your copyright notice in the Description page of Project Settings.

#include "SokobanCell.h"

#include "Components/BoxComponent.h"

ASokobanCell::ASokobanCell()
{
	PrimaryActorTick.bCanEverTick = false;


	const FName SceneRootName = TEXT("SceneRoot");

	SetRootComponent(
		CellRoot = CreateDefaultSubobject<UBoxComponent>(SceneRootName)
	);


	const FName CellBasePivotName = TEXT("BasePivot");

	CellBasePivot = CreateDefaultSubobject<USceneComponent>(CellBasePivotName);
	CellBasePivot->SetupAttachment(CellRoot);


	const FName CellFeaturePivotName = TEXT("FeaturesPivot");

	CellFeaturePivot = CreateDefaultSubobject<USceneComponent>(CellFeaturePivotName);
	CellFeaturePivot->SetupAttachment(CellRoot);
	CellFeaturePivot->AddRelativeLocation(FVector{ 0.f, 0.f, Size });

}
