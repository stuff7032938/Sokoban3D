// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SokobanBall.h"
#include "SokobanBox.h"
#include "SokobanCell.h"

#include "GameFramework/Actor.h"
#include "SokobanMap.generated.h"

USTRUCT(BlueprintType)
struct FMove
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly)
	ASokobanCell* OriginCell;

	UPROPERTY(BlueprintReadOnly)
	ASokobanCell* TargetCell;

};

UCLASS(Abstract)
class SOKOBAN3D_API ASokobanMap final : public AActor
{
	GENERATED_BODY()

	friend ASokobanBall;
	friend ASokobanBox;

public:

	ASokobanMap();

protected:

	virtual void BeginPlay() override;

private:

	UFUNCTION(CallInEditor, Category="Map Base Generator")
	void GenerateMapBase();

	UFUNCTION(BlueprintCallable)
	void UndoMove();


	ASokobanCell* & CellAt(int32 X, int32 Y);

public:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* MapRoot;


	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="Map Base Generator")
	TSubclassOf<ASokobanCell> CellClass;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="Map Base Generator")
	TSubclassOf<ASokobanBox> BoxClass;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category="Map Base Generator")
	TSubclassOf<ASokobanBall> BallClass;


	UPROPERTY(BlueprintReadOnly, EditInstanceOnly, Category="Map Base Generator", meta = (ClampMin = "0"))
	int32 MapHeight = 9;

	UPROPERTY(BlueprintReadOnly, EditInstanceOnly, Category="Map Base Generator", meta = (ClampMin = "0"))
	int32 MapWidth = 8;

	UPROPERTY(BlueprintReadOnly)
	int32 MapSize = MapHeight * MapWidth;

	UPROPERTY(BlueprintReadOnly)
	float CellSize;


	UPROPERTY(BlueprintReadOnly)
	TArray<ASokobanCell*> Cells;


	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	TArray<ASokobanBox*> Boxes;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	ASokobanBall* Ball;


	UPROPERTY(BlueprintReadOnly)
	TArray<FMove> Moves;

};
