// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SokobanBox.generated.h"

enum class EMovementDirection : uint8;

UCLASS(Abstract)
class SOKOBAN3D_API ASokobanBox final : public AActor
{
	GENERATED_BODY()

	friend class ASokobanMap;

public:

	ASokobanBox();


	virtual void Tick(float DeltaSeconds) override;


	bool Push(EMovementDirection Direction);

private:

	void MoveTo(class ASokobanCell* Target);

public:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class UStaticMeshComponent* BoxRoot;


	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	class ASokobanMap* Map;

	UPROPERTY(BlueprintReadOnly)
	int32 X;

	UPROPERTY(BlueprintReadOnly)
	int32 Y;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	bool bIsInPlace;


	UPROPERTY(BlueprintReadOnly)
	FVector Destination;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (ClampMin = "0"))
	float Speed;

};
