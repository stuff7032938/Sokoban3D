// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class Sokoban3D : ModuleRules
{
	public Sokoban3D(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange( new[] { "Core", "CoreUObject", "Engine", "InputCore" } );

	}
}
