// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SokobanGame.generated.h"

UCLASS(Abstract)
class SOKOBAN3D_API ASokobanGame final : public AGameModeBase
{
	GENERATED_BODY()

public:

	ASokobanGame();


	virtual void Tick(float DeltaSeconds) override;

public:

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	class ASokobanMap* Map;


	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	int32 Score = 0;

	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	int32 MaxScore;

};
