// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SokobanCell.generated.h"

UENUM(BlueprintType)
enum class ECellType : uint8
{
	Empty, Floor, Wall, Place, Box, Ball
};

UCLASS(Abstract)
class SOKOBAN3D_API ASokobanCell final : public AActor
{
	GENERATED_BODY()

public:

	ASokobanCell();

public:

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	ECellType Type = ECellType::Empty;

	UPROPERTY(BlueprintReadOnly)
	AActor* Occupant;


	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class UBoxComponent* CellRoot;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* CellBasePivot;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* CellFeaturePivot;


	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	float Size = 100.f;

	UPROPERTY(BlueprintReadOnly)
	int32 X;

	UPROPERTY(BlueprintReadOnly)
	int32 Y;

};
