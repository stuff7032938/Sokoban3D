// Fill out your copyright notice in the Description page of Project Settings.

#include "SokobanBall.h"

#include "SokobanMap.h"

ASokobanBall::ASokobanBall()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;


	Speed = 300.f;


	const FName SceneRootName = TEXT("BallRoot");

	SetRootComponent(
		Root = CreateDefaultSubobject<USceneComponent>(SceneRootName)
	);

	const FName MeshName = TEXT("BallMesh");

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(MeshName);
	Mesh->SetupAttachment(Root);

	const FName BoomName = TEXT("CameraBoom");

	Boom = CreateDefaultSubobject<USpringArmComponent>(BoomName);
	Boom->SetupAttachment(Mesh);

	const FName CameraName = TEXT("Camera");

	Camera = CreateDefaultSubobject<UCameraComponent>(CameraName);
	Camera->SetupAttachment(Boom);

	AutoPossessPlayer = EAutoReceiveInput::Player0;

}

void ASokobanBall::Tick(const float DeltaSeconds)
{
	const FVector NewLocation =
		FMath::VInterpConstantTo(GetActorLocation(), Destination, DeltaSeconds, Speed);
	SetActorLocation(NewLocation);

	const FRotator RotationDirection = {
		-CurrentDirection.X, 0.f, CurrentDirection.Y
	};

	const FRotator DeltaRotation =
		RotationDirection * 180.f * (Speed / Map->CellSize) * DeltaSeconds;
	Mesh->AddWorldRotation(DeltaRotation);

	if (Destination.Equals(NewLocation))
	{
		SetActorTickEnabled(false);
	}

}

void ASokobanBall::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("Up"), IE_Pressed, this, &ASokobanBall::MoveUp);
	PlayerInputComponent->BindAction(TEXT("Down"), IE_Pressed, this, &ASokobanBall::MoveDown);
	PlayerInputComponent->BindAction(TEXT("Left"), IE_Pressed, this, &ASokobanBall::MoveLeft);
	PlayerInputComponent->BindAction(TEXT("Right"), IE_Pressed, this, &ASokobanBall::MoveRight);

}

void ASokobanBall::Move(const EMovementDirection Direction)
{
	if (IsActorTickEnabled())
		return;

	int32 TargetX = X, TargetY = Y;

	switch (Direction)
	{
	case EMovementDirection::Up:
		TargetY -= 1;
		break;
	case EMovementDirection::Down:
		TargetY += 1;
		break;
	case EMovementDirection::Left:
		TargetX -= 1;
		break;
	case EMovementDirection::Right:
		TargetX += 1;
		break;
	default:
		break;
	}

	ASokobanCell* OriginCell = Map->CellAt(X, Y);
	ASokobanCell* TargetCell = Map->CellAt(TargetX, TargetY);

	const bool NotAWall =
		TargetCell->Type == ECellType::Place ||
		TargetCell->Type == ECellType::Floor;
	if (NotAWall)
	{
		if (ASokobanBox* Box = Cast<ASokobanBox>(TargetCell->Occupant))
		{
			Map->Moves.Push(
				FMove{ OriginCell, TargetCell }
			);

			if (Box->Push(Direction))
			{
				MoveTo(TargetCell);
			}
			else
			{
				Map->Moves.Pop();
			}
		}
		else if (!TargetCell->Occupant)
		{
			Map->Moves.Push(
				FMove{ OriginCell, TargetCell }
			);

			MoveTo(TargetCell);
		}
	}

}

void ASokobanBall::MoveTo(ASokobanCell* Target)
{
	Map->CellAt(X, Y)->Occupant = nullptr;
	Target->Occupant = this;

	X = Target->X;
	Y = Target->Y;

	Destination = Map->GetActorLocation() + FVector{
		X * Target->Size,
		Y * Target->Size, Target->Size };
	CurrentDirection = (Destination - GetActorLocation());
	CurrentDirection.Normalize();

	SetActorTickEnabled(true);

}
