// Fill out your copyright notice in the Description page of Project Settings.

#include "SokobanBox.h"

#include "SokobanBall.h"
#include "SokobanCell.h"
#include "SokobanMap.h"

ASokobanBox::ASokobanBox()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;


	Speed = 300.f;

	bIsInPlace = false;


	const FName SceneRootName = TEXT("BoxRoot");

	SetRootComponent(
        BoxRoot = CreateDefaultSubobject<UStaticMeshComponent>(SceneRootName)
    );

}

void ASokobanBox::Tick(const float DeltaSeconds)
{
	const FVector NewLocation =
		FMath::VInterpConstantTo(GetActorLocation(), Destination, DeltaSeconds, Speed);
	SetActorLocation(NewLocation);

	if (Destination.Equals(NewLocation))
	{
		SetActorTickEnabled(false);
	}

}

bool ASokobanBox::Push(const EMovementDirection Direction)
{
	int32 TargetX = X, TargetY = Y;

	switch (Direction)
	{
	case EMovementDirection::Up:
		TargetY -= 1;
		break;
	case EMovementDirection::Down:
		TargetY += 1;
		break;
	case EMovementDirection::Left:
		TargetX -= 1;
		break;
	case EMovementDirection::Right:
		TargetX += 1;
		break;
	default:
		break;
	}

	ASokobanCell* OriginCell = Map->CellAt(X, Y);
	ASokobanCell* TargetCell = Map->CellAt(TargetX, TargetY);

	const bool IsAPlace = TargetCell->Type == ECellType::Place;

	const bool NotAWall = IsAPlace ||
		TargetCell->Type == ECellType::Floor;
	if (NotAWall)
	{
		if (!TargetCell->Occupant)
		{
			Map->Moves.Push(
				FMove{ OriginCell, TargetCell }
			);

			MoveTo(TargetCell);

			bIsInPlace = IsAPlace;

			return true;
		}
	}

	return false;
}

void ASokobanBox::MoveTo(ASokobanCell* Target)
{
	Map->CellAt(X, Y)->Occupant = nullptr;
	Target->Occupant = this;

	X = Target->X;
	Y = Target->Y;

	Destination = Map->GetActorLocation() + FVector{
		X * Target->Size,
		Y * Target->Size, Target->Size
	};

	SetActorTickEnabled(true);

}
