// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Pawn.h"
#include "SokobanBall.generated.h"

UENUM(BlueprintType)
enum class EMovementDirection : uint8
{
	None, Up, Down, Left, Right
};

UCLASS(Abstract)
class SOKOBAN3D_API ASokobanBall final : public APawn
{
	GENERATED_BODY()

	friend class ASokobanMap;

public:

	ASokobanBall();


	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:

	void Move(EMovementDirection Direction);


	void MoveUp() { Move(EMovementDirection::Up); }
	void MoveDown() { Move(EMovementDirection::Down); }
	void MoveLeft() { Move(EMovementDirection::Left); }
	void MoveRight() { Move(EMovementDirection::Right); }


	void MoveTo(class ASokobanCell* Target);

public:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USceneComponent* Root;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USpringArmComponent* Boom;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class UCameraComponent* Camera;


	UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
	class ASokobanMap* Map;

	UPROPERTY(BlueprintReadOnly)
	int32 X;

	UPROPERTY(BlueprintReadOnly)
	int32 Y;


	UPROPERTY(BlueprintReadOnly)
	FVector Destination;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta = (ClampMin = "0"))
	float Speed;

	UPROPERTY(BlueprintReadOnly)
	FVector CurrentDirection;

};
