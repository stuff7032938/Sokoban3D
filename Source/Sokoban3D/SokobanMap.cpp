// Fill out your copyright notice in the Description page of Project Settings.

#include "SokobanMap.h"

#include "SokobanGame.h"

#include "Kismet/GameplayStatics.h"

ASokobanMap::ASokobanMap()
{
	PrimaryActorTick.bCanEverTick = false;


	const FName SceneRootName = TEXT("MapRoot");

	SetRootComponent(
		MapRoot = CreateDefaultSubobject<USceneComponent>(SceneRootName)
	);

}

void ASokobanMap::BeginPlay()
{
	Super::BeginPlay();

	auto* Game = Cast<ASokobanGame>(
		UGameplayStatics::GetGameMode(GetWorld())
	);
	Game->Map = this;

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	for (ASokobanCell* Cell : Cells)
	{
		switch (Cell->Type)
		{
			case ECellType::Box:
				if (BoxClass) {
					ASokobanBox* NewBox = GetWorld()->SpawnActor<ASokobanBox>(BoxClass, SpawnParameters);
					NewBox->Map = this;
					NewBox->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

					const FVector BoxLocation{
						Cell->X * CellSize,
						Cell->Y * CellSize, CellSize };
					NewBox->SetActorRelativeLocation(BoxLocation);

					NewBox->X = Cell->X;
					NewBox->Y = Cell->Y;

					Boxes.Add(NewBox);

					Cell->Type = ECellType::Floor;
					Cell->Occupant = NewBox;
				}
				break;
			case ECellType::Ball:
				if (BallClass) {
					Ball = GetWorld()->SpawnActor<ASokobanBall>(BallClass, SpawnParameters);
					Ball->Map = this;
					Ball->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

					const FVector BallLocation{
						Cell->X * CellSize,
						Cell->Y * CellSize, CellSize };
					Ball->SetActorRelativeLocation(BallLocation);

					Ball->X = Cell->X;
					Ball->Y = Cell->Y;

					Cell->Type = ECellType::Floor;
					Cell->Occupant = Ball;
				}
				break;
			default:
				break;
		}
	}

	Game->MaxScore = Boxes.Num();

}

void ASokobanMap::GenerateMapBase()
{
	if (!CellClass)
		return;

	for (ASokobanCell* Cell : Cells) {
		Cell->Destroy();
	}

	Cells.SetNumZeroed(MapSize = MapHeight * MapWidth);

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ASokobanCell* DefaultCell = Cast<ASokobanCell>(CellClass->GetDefaultObject());
	CellSize = DefaultCell->Size;

	for (int Y = 0; Y < MapHeight; ++Y) {
		for (int X = 0; X < MapWidth; ++X) {

			ASokobanCell* NewCell = GetWorld()->SpawnActor<ASokobanCell>(CellClass, SpawnParameters);
			NewCell->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

			NewCell->X = X;
			NewCell->Y = Y;

			const FVector CellLocation{
				X * CellSize,
				Y * CellSize, 0.f };
			NewCell->SetActorRelativeLocation(CellLocation);

			#if WITH_EDITOR
			 FString NewCellLabel = TEXT("Cell_");

			 NewCellLabel.AppendInt(X);
			 NewCellLabel += TEXT(";");
			 NewCellLabel.AppendInt(Y);

			 NewCell->SetActorLabel(NewCellLabel);
			#endif

			CellAt(X, Y) = NewCell;
		}
	}

}

void ASokobanMap::UndoMove()
{
	if (!Moves.Num() || Ball->IsActorTickEnabled())
		return;

	const FMove Move = Moves.Pop();

	if (auto Sokoban = Cast<ASokobanBall>(Move.TargetCell->Occupant))
	{
		Sokoban->MoveTo(Move.OriginCell);
	}
	else if (auto Box = Cast<ASokobanBox>(Move.TargetCell->Occupant))
	{
		const FMove PusherMove = Moves.Pop();

		auto Pusher =
			Cast<ASokobanBall>(PusherMove.TargetCell->Occupant);
		Pusher->MoveTo(PusherMove.OriginCell);

		Box->MoveTo(Move.OriginCell);
	}

}

ASokobanCell* & ASokobanMap::CellAt(const int32 X, const int32 Y)
{
	int32 Index = X + MapWidth*Y;

	if (Index >= MapSize)
		Index = 0;
	else if (Index < 0)
		Index = MapSize - 1;

	return Cells[Index];
}
