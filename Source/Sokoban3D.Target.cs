// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class Sokoban3DTarget : TargetRules
{
	public Sokoban3DTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new[] { "Sokoban3D" } );
	}
}
