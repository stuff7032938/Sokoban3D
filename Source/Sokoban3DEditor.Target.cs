// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;

public class Sokoban3DEditorTarget : TargetRules
{
	public Sokoban3DEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new[] { "Sokoban3D" } );
	}
}
